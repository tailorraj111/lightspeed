import frappe
from frappe.utils import cint, flt
from frappe import _
import json
import requests

@frappe.whitelist()
def after_insert(self,method):
    # frappe.msgprint("Creating Item On Ecomm")
    api_key = frappe.db.get_value("Light Speed Credentials",None,"api_key")
    api_secret = frappe.db.get_value("Light Speed Credentials",None,"api_secret")
    if not api_key or not api_secret:
        raise frappe.ValidationError(_("Light Speed Is Not Confugured"))
    # frappe.msgprint()
    data = {
    'product[visibility]': 'visible',
    'product[data01]': '',
    'product[data02]': '',
    'product[data03]': '',
    'product[title]': self.item_code,
    'product[fulltitle]': self.item_name,
    'product[description]': self.description,
    'product[content]': self.website_content,
    
    'product[supplier]': self.supplier_name,
    'product[brand]': self.brand
    }
    
    if not self.created_from_website:
        try:
            response = requests.post('https://api.webshopapp.com/nl/products.json', data=data, auth=(api_key, api_secret))
            if response.text:
                frappe.msgprint("Item Creared On Website")
        except requests.exceptions.HTTPError:
            frappe.throw(_("Could not create Item On Light Speed!"))

    # data = {
    # 'product[visibility]': 'visible',
    # 'product[data01]': '',
    # 'product[data02]': '',
    # 'product[data03]': '',
    # 'product[title]': 'Lookin\' Sharp T-Shirt',
    # 'product[fulltitle]': 'Lookin\' Sharp T-Shirt',
    # 'product[description]': 'Description of the Lookin\' Sharp T-Shirt',
    # 'product[content]': '<p>Long Description of the Lookin\' Sharp T-Shirt</p>',
    # 'product[deliverydate]': '6488',
    # 'product[supplier]': '78794',
    # 'product[brand]': '1171202'
    # }

    # response = requests.post('https://api.webshopapp.com/nl/products.json', data=data, auth=(api_key, api_secret))
    # frappe.msgprint(str(response.text))

    

